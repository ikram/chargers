<section class="slide  slide--dark  slide-facts" id="facts">
    <div class="container  text--center">
        <div class="push-xl--bottom  soft-sm--bottom">
            <h2 class="text--six  text--normal  text--muted  text--uppercase  push-xs--bottom" style="font-family: inherit; letter-spacing: 1px">A strong partnership</h2>
            <h3 class="text--three  text--bold">In collaboration with BMW ChargeNow, ChargEV makes the EV charging process simple and easy</h3>
        </div>

        <div class="js-grid-facts  grid  grid--facts  grid--full">
            <div class="grid__item  one-third  soft-xl  palm-one-whole">
                <h3 class="text--six  text--muted  text--uppercase  push-sm--bottom">Public EV Chargers</h3>
                <div class="js-animate-number  text--one  palm-text--three  text--light" data-number="50">0</div>
                <div class="milli  text--muted">Stations</div>
            </div>

            <div class="grid__item  one-third  soft-xl  palm-one-whole  grid--facts__border-left">
                <h3 class="text--six  text--muted  text--uppercase  push-sm--bottom">Public EV Charging</h3>
                <div class="js-animate-number  text--one  palm-text--three  text--light" data-number="31">0</div>
                <div class="milli  text--muted">Location available in Malaysia</div>
            </div>


            <div class="grid__item  one-third  soft-xl  palm-one-whole  grid--facts__border-left">
                <h3 class="text--six  text--muted  text--uppercase  push-sm--bottom">EV Charging Sessions</h3>
                <div class="js-animate-number  text--one  palm-text--three  text--light" data-number="3200">0</div>
                <div class="milli  text--muted">Sessions</div>
            </div>

            <div class="grid__item  one-third  soft-xl  palm-one-whole  grid--facts__border-top">
                <h3 class="text--six  text--muted  text--uppercase  push-sm--bottom">Electricity Charged</h3>
                <div class="js-animate-number  text--one  text--light" data-number="13250">0</div>
                <div class="milli  text--muted">Kilowatt hour</div>
            </div>

            <div class="grid__item  one-third  soft-xl  palm-one-whole  grid--facts__border-top  grid--facts__border-left">
                <h3 class="text--six  text--muted  text--uppercase  push-sm--bottom">Estimated Electric Range</h3>
                <div class="js-animate-number  text--one  palm-text--three  text--light" data-number="106000">0</div>
                <div class="milli  text--muted">Kilometres</div>
            </div>

            <div class="grid__item  one-third  soft-xl  palm-one-whole  grid--facts__border-top  grid--facts__border-left">
                <h3 class="text--six  text--muted  text--uppercase  push-sm--bottom">Estimated CO2 Emissions Avoided</h3>
                <div class="js-animate-number  text--one  text--light" data-number="5300">0</div>
                <div class="milli  text--muted">Kilograms</div>
            </div>

            <div class="grid__item  one-half  soft-xl  palm-one-whole  grid--facts__border-top">
                <h3 class="text--six  text--muted  text--uppercase  push-sm--bottom">Semi-public EV Chargers</h3>
                <div class="js-animate-number  text--one  palm-text--three  text--light" data-number="69">0</div>
                <div class="milli  text--muted">Units</div>
            </div>

            <div class="grid__item  one-half  soft-xl  palm-one-whole  grid--facts__border-top  grid--facts__border-left">
                <h3 class="text--six  text--muted  text--uppercase  push-sm--bottom">Installed with EV Chargers</h3>
                <div class="js-animate-number  text--one  text--light" data-number="24">0</div>
                <div class="milli  text--muted">Condominiums</div>
            </div>
        </div>
    </div>

    <img src="images/wave.svg" style="width: 100%; max-width: 200%; display: none">
</section>
