<code style="font-size: 11px;">
<?php
// version 1.0
class App_File_Zip_Exception extends Exception {}
class App_File_Zip {

    /**
     * Zip a file, or entire directory recursively.
     *
     * @param string $source directory or file name
     * @param string $destinationPathAndFilename full path to output
     * @throws App_File_Zip_Exception
     * @return boolean whether zip was a success
     */
    public static function CreateFromFilesystem($source, $destinationPathAndFilename)
    {
        $base = realpath(dirname($destinationPathAndFilename));

        if (!is_writable($base))
        {
            throw new App_File_Zip_Exception('Destination must be writable directory.');
        }

        if (!is_dir($base))
        {
            throw new App_File_Zip_Exception('Destination must be a writable directory.');
        }

        if (!file_exists($source))
        {
            throw new App_File_Zip_Exception('Source doesnt exist in location: ' . $source);
        }

        $source = realpath($source);

        if (!extension_loaded('zip') || !file_exists($source))
        {
            return false;
        }

        $zip = new ZipArchive();
        $zip->open($destinationPathAndFilename, ZipArchive::CREATE);
        if (is_dir($source) === true)
        {
            $files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($source), RecursiveIteratorIterator::SELF_FIRST);
            $baseName = realpath($source);
            foreach ($files as $file)
            {
                if( in_array(substr($file, strrpos($file, DIRECTORY_SEPARATOR)+1), array('.', '..')) )
                {
                    continue;
                }

                $relative = substr($file, strlen($baseName));
                if (is_dir($file) === true)
                {
                    // Add directory
                    $added = $zip->addEmptyDir(trim($relative, "\\/"));
                    if (!$added)
                    {
                        throw new App_File_Zip_Exception('Unable to add directory named: ' . trim($relative, "\\/"));
                    }
                }
                else if (is_file($file) === true)
                {
                    // Add file
                    $added = $zip->addFromString(trim($relative, "\\/"), file_get_contents($file));
                    if (!$added)
                    {
                        throw new App_File_Zip_Exception('Unable to add file named: ' . trim($relative, "\\/"));
                    }
                }
            }
        }
        else if (is_file($source) === true)
        {
            // Add file
            $added = $zip->addFromString(trim(basename($source), "\\/"), file_get_contents($source));
            if (!$added)
            {
                throw new App_File_Zip_Exception('Unable to add file named: ' . trim($relative, "\\/"));
            }
        }
        else
        {
            throw new App_File_Zip_Exception('Source must be a directory or a file.');
        }

        $zip->setArchiveComment('Created with App_Framework');

        return $zip->close();
    }

}

function recurse_copy($src,$dst) {
    $dir = opendir($src);
    @mkdir($dst);
    while(false !== ( $file = readdir($dir)) ) {
        if (( $file != '.' ) && ( $file != '..' ) && $file != '.hg') {
            if ( is_dir($src . '/' . $file) ) {
                recurse_copy($src . '/' . $file,$dst . '/' . $file);
            }
            else {
                copy($src . '/' . $file,$dst . '/' . $file);

                echo $file .'</br>';
                // Change all .php text into .html text, if the file is php file
                if(  (strlen($file) > 5) && (substr($file, -4, 4) == '.php' || substr($file, -5, 5) == '.html') ){
                    echo "<span style='color: green;'>doing curl : </span>";
                    $curlPath = $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
                    $curlPath = str_replace('build.php', $file, $curlPath);
                    // create a new cURL resource
                    $ch = curl_init();

                    // set URL and other appropriate options
                    curl_setopt($ch, CURLOPT_URL, $curlPath);
                    curl_setopt($ch, CURLOPT_HEADER, 0);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                    // grab URL and pass it to the browser
                    $content = curl_exec($ch);
                    //echo $content; exit;
                    // close cURL resource, and free up system resources
                    curl_close($ch);
                    file_put_contents($dst . '/' . $file, $content);
                }

                $content=str_replace('.php', '.html', file_get_contents($dst . '/' . $file));
                file_put_contents($dst . '/' . $file, $content);
            }
        }
    }
    closedir($dir);
}

# recursively remove a directory
function deleteDir($dirPath) {
    if (! is_dir($dirPath)) {
        throw new InvalidArgumentException("$dirPath must be a directory");
    }
    if (substr($dirPath, strlen($dirPath) - 1, 1) != '/') {
        $dirPath .= '/';
    }
    $files = glob($dirPath . '*', GLOB_MARK);
    foreach ($files as $file) {
        if (is_dir($file)) {
            deleteDir($file);
        } else {
            unlink($file);
        }
    }
    rmdir($dirPath);
}


// simple script to build and package everything in this folder

// Delete any existing build folder
if( is_dir('package')){
    deleteDir('package');
}
mkdir('package');

//print_r($_SERVER);exit;

// Copy all non php files
if ($handle = opendir(dirname(__FILE__))) {
    echo "Directory handle: $handle\n";
    echo "Entries:\n";

    /* This is the correct way to loop over the directory. */
    while (false !== ($entry = readdir($handle))) {

        if( $entry =='build.php'
            || $entry == '.'
            || $entry == '..'
            || $entry == 'package'){
            // Do nothing
            echo "DO nothing </br>";
        } else if( is_dir($entry) && ($entry != '.hg')){
            echo "<hr style='border: 0; border-top: 1px solid #eee; margin: 15px 0;'><span style='color: blue;'>copying folder $entry</span></br>";
            recurse_copy($entry , 'package/'.$entry);
        } else if( is_file($entry)){

            // skip file that start with _
            if($entry[0] == '_' || $entry == '.DS_Store') {
                //skip
            } else {
       //       ob_start();
       //       include($entry);
       //       $content = ob_get_contents();
                // ob_end_clean();
                echo $entry;
                echo "<br><span style='color: green;'>doing curl : </span>";

                $curlPath = $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
                $curlPath = str_replace('build.php', $entry, $curlPath);
                // create a new cURL resource
                $ch = curl_init();

                // set URL and other appropriate options
                curl_setopt($ch, CURLOPT_URL, $curlPath);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

                // grab URL and pass it to the browser
                $content = curl_exec($ch);

                // close cURL resource, and free up system resources
                curl_close($ch);

                $content = str_replace(".php", ".html", $content);

                if( substr($entry, -4, 4) == '.php') {
                    file_put_contents( 'package/'.substr($entry,0, strlen($entry) - 3).'html' , $content);
                } else if(substr($entry, -5, 5) == '.html'){
                    file_put_contents( 'package/'.substr($entry,0, strlen($entry) - 4).'html' , $content);
                }
            }
        }
    }

    closedir($handle);
}

$packageName = basename(dirname(__FILE__)). '_'.date('dmY');;
$packageName = dirname(__FILE__) .'/'.$packageName. '.zip';
unlink($packageName);
try {
    $source = dirname(__FILE__) .'/package';
    $destination = $packageName;
    App_File_Zip::CreateFromFilesystem($source, $destination);
} catch (App_File_Zip_Exception $e) {
    // Zip file was not created.
}
?>
</code>