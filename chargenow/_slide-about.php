<section class="slide  slide-about" style="background: #FFF" id="about">
    <div class="container  text--center">
        <div class="push-xl--bottom  soft-sm--bottom">
            <h2 class="text--six  text--normal  text--muted  text--uppercase  push-xs--bottom" style="font-family: inherit; letter-spacing: 1px">About Us</h2>
            <h3 class="text--three  text--bold">Short introduction to this collaboration</h3>
        </div>

        <div class="grid  grid--about  grid--xl  text--left" style="text-align: left !important">
            <div class="grid__item  one-half  palm-one-whole">
                <div class="soft-xl  hard--top">
                    <div style="border: 1px solid #e5e5e5; padding: 0 100px; margin: 0 0 16px;">
                        <img src="images/billboard-chargenow.png">
                    </div>
                    <p class="lead">ChargeNow is a mobility service from BMW and the largest public charging network in Malaysia.</p>
                    <?php /**/?>
                    <p>The charging stations in the ChargeNow network are displayed using the BMW ConnectedDrive in-vehicle navigation console, along with its real-time status, pricing info and turn-by-turn directions.</p>
                    <div>With ChargeNow, BMW i and iPerformance drivers will have easy access to the public charging stations located across Malaysia.</div>
                </div>
            </div>

            <div class="grid__item  one-half  palm-one-whole">
                <div class="soft-xl  hard--top">
                    <div style="border: 1px solid #e5e5e5; padding: 0 100px; margin: 0 0 16px;">
                        <img src="images/billboard-chargev-greentech.png">
                    </div>
                    <p class="lead">ChargEV is a leading provider of electric vehicle charging solutions in Malaysia.</p>
                    <p>Specializing in open standards, cloud-based platforms and mobile applications, ChargEV has also installed 160 charging stations at more than 70 locations across the country.</p>
                    <div>In collaboration with BMW ChargeNow, ChargEV makes the EV charging process simple and easy, as it should be.</div>
                </div>
            </div>
        </div>

        <hr class="push-xl--bottom">

        <div class="grid  grid--about  grid--xl  text--left" style="text-align: justify !important">
            <div class="grid__item  one-half  palm-one-whole">
                <div class="soft-xl  hard--top">
                    <p>BMW Group Malaysia and Malaysian Green Technology Corporation (GreenTech) officially marked their partnership at the 7th International Greentech & Eco Products Exhibition and Conference Malaysia (IGEM), to offer the ChargeNow mobility service via GreenTech’s ChargEV platform.</p>
                </div>
            </div>

            <div class="grid__item  one-half  palm-one-whole">
                <div class="soft-xl  hard--top">
                    <div>This initiative is part of the BMW 360° ELECTRIC program and allows owners of BMW plug-in hybrid and electric vehicles like the i8, 330e and X5 xDrive40e on-the-go charging. With the BMW ChargeNow  card, owners of these vehicles get access to the ChargEV partner charging stations 24/7.</div>
                </div>
            </div>
        </div>

        <div class="js-car-slides  luxurious-bmw-cars  visuallyhidden--palm">
            <img src="images/BMW-i8.png" class="tier--four">
            <img src="images/BMW-X5.png" class="tier--three">
            <img src="images/BMW-7.png" class="tier--two">
            <img src="images/BMW-3.png" class="tier--one">
        </div>
    </div>
</section>
