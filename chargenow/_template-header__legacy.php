<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

    <!-- Page title for SEO -->
    <title>ChargeNow Malaysia</title>
    <meta name="description" content="ChargeNow is a mobility service from BMW and the largest public charging network in Malaysia.">

    <!-- Open Graph content -->
    <meta property="”og:title”" content="ChargeNow">
    <meta property="og:description" content="ChargeNow is a mobility service from BMW and the largest public charging network in Malaysia.">
    <meta property="og:image" content="og-image.png">

    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700|Rubik:300" rel="stylesheet">
    <link rel="stylesheet" href="styles/assets.css">
</head>
<body class="text--light">
    <header class="header  text--center">
        <div class="container  text--left  soft-lg--ends">
            <div class="table  one-whole">
                <div class="table-cell  valign--middle  one-whole">
                    <img src="images/logo-chargenow.png" class="header__logo  header__logo--main  float--left  palm-two-fifhts" height="64" />
                </div>

                <div class="table-cell  valign--middle  one-whole  visuallyhidden--palm  space--nowrap">
                    <img src="images/logo-chargev.png" class="inline--block  valign--top  push-sm--top  push-lg--right  palm-two-fifhts  visuallyhidden--palm" height="48" />
                    <img src="images/logo-greentech.png" class="inline--block  valign--top  push-sm--top  palm-two-fifhts  visuallyhidden--palm" height="48" />
                </div>

                <div class="table-cell  valign--middle  visuallyhidden--lap-and-up">
                    <a href="javascript:void(0)" class="header__menu-mobile  push-xl--right  js-toggle-menu">
                        <span class="bars  relative  transition--default">
                            <span class="bar  bar--one  absolute  transition--default"></span>
                            <span class="bar  bar--two  absolute  transition--default"></span>
                            <span class="bar  bar--tri  absolute  transition--default"></span>
                        </span>
                    </a>
                </div>
            </div>
        </div>

        <div class="header__menu  transition--default">
            <div class="container">
                <ul class="menu  text--uppercase">
                    <li class="palm-one-whole"><a href="#">About</a></li>
                    <li class="palm-one-whole"><a href="#">News</a></li>
                    <li class="palm-one-whole"><a href="#">Getting Started</a></li>
                    <li class="palm-one-whole"><a href="#">Locations</a></li>
                    <li class="palm-one-whole"><a href="#">FAQs</a></li>
                </ul>
            </div>
        </div>
    </header>
