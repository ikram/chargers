<section class="slide  slide-facts" style="background: #FFF" id="news">
    <div class="container  text--center">
        <div class="push-xl--bottom  soft-sm--bottom">
            <h2 class="text--six  text--normal  text--muted  text--uppercase  push-xs--bottom" style="font-family: inherit; letter-spacing: 1px">Latest Updates</h2>
            <h3 class="text--three  text--bold">News and updates about us</h3>
        </div>

        <div class="grid  grid--xl  text--left">
            <div class="grid__item  one-half  palm-one-whole">
                <div class="news  grid  push-xl--bottom  soft-xl--bottom">
                    <div class="grid__item  one-third">
                        <div style="background-color: #EEE; padding-bottom: 75%; background-image: url('https://s2.paultan.org/image/2016/10/BMW-GreenTech-IGEM-4-630x420.jpg')" class="cover"></div>
                    </div>
                    <div class="grid__item  two-thirds">
                        <h4 class="text--five  flush"><a href="#">BMW Group Malaysia strengthens partnership with GreenTech – 1,000 ChargEV stations by end-2017</a></h4>
                        <div class="milli  text--muted  push-sm--top">
                            5 October 2016. Source: Paul Tan
                        </div>
                    </div>
                </div>
            </div>

            <div class="grid__item  one-half  palm-one-whole">
                <div class="news  grid  push-xl--bottom  soft-xl--bottom">
                    <div class="grid__item  one-third">
                        <div style="background-color: #EEE; padding-bottom: 75%; background-image: url('https://edgemarkets.s3-ap-southeast-1.amazonaws.com/pictures/BMW-330e_5Oct16_theedgemarkets.jpg')" class="cover"></div>
                    </div>
                    <div class="grid__item  two-thirds">
                        <h4 class="text--five  flush"><a href="#">BMW Group M'sia customers now can recharge their vehicles at GreenTech's ChargEV platform</a></h4>
                        <div class="milli  text--muted  push-sm--top">
                            5 October 2016. Source: The Edge Markets
                        </div>
                    </div>
                </div>
            </div>

            <div class="grid__item  one-half  palm-one-whole">
                <div class="news  grid  push-xl--bottom  soft-xl--bottom">
                    <div class="grid__item  one-third">
                        <div style="background-color: #EEE; padding-bottom: 75%; background-image: url('https://s1.paultan.org/image/2016/10/BMW-GreenTech-IGEM-3-850x567.jpg')" class="cover"></div>
                    </div>
                    <div class="grid__item  two-thirds">
                        <h4 class="text--five  flush"><a href="#">BMW PHEV owners get access to ChargeNow</a></h4>
                        <div class="milli  text--muted  push-sm--top">
                            1 October 2016. Source: The Malaysian Reserve
                        </div>
                    </div>
                </div>
            </div>

            <div class="grid__item  one-half  palm-one-whole">
                <div class="news  grid  push-xl--bottom  soft-xl--bottom">
                    <div class="grid__item  one-third">
                        <div style="background-color: #EEE; padding-bottom: 75%; background-image: url('https://content.icarcdn.com/styles/article_cover/s3/field/article/cover/2016/bmw_01.jpg?itok=_fCOrwI7')" class="cover"></div>
                    </div>
                    <div class="grid__item  two-thirds">
                        <h4 class="text--five  flush"><a href="#">BMW Group Malaysia Amps Up Electric Drive Push With ChargEV</a></h4>
                        <div class="milli  text--muted  push-sm--top">
                            1 October 2016. Source: Carlist.my
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
