<section class="slide  slide--dark  slide-contact">
    <div class="container  text--center">
        <h2 class="text--six  text--normal  text--muted  text--uppercase  push-xs--bottom" style="font-family: inherit; letter-spacing: 1px">Register Now</h2>
        <h3 class="text--three  text--bold">Ready to signup?</h3>

        <div class="text--center">
            <p>You may register your new account using the web portal</p>

            <a href="register.php" class="btn  btn--secondary  text--uppercase" style="border-radius: 100px; padding: 12px 32px;">Register Now</a>
        </div>
    </div>
</section>
