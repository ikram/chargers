<section class="slide  slide-steps" id="getting-started">
    <div class="container  text--center">
        <div class="push-xl--bottom  soft-sm--bottom">
            <h2 class="text--six  text--normal  text--muted  text--uppercase  push-xs--bottom" style="font-family: inherit; letter-spacing: 1px">Getting Started</h2>
            <h3 class="text--three  text--bold">Four easy steps to get charged</h3>
        </div>

        <div class="grid  grid--steps  grid--lg">
            <div class="grid__item  one-quarter  palm-one-whole">
                <div class="card">
                    <div class="cover  soft-sm--ends" style="background-image: url('icons/presentation/cloud.svg'); background-size: 100px">
                        <img src="icons/presentation/icons8-id_verified.svg" width="48">
                    </div>
                    <div class="soft-lg  push-sm--top">
                        <h3 class="text--five  text--bold  text--uppercase  push-sm--bottom">Register</h3>
                        <div>Sign up for a new account easily with your email address</div>
                    </div>
                </div>
            </div>
            <div class="grid__item  one-quarter  palm-one-whole">
                <div class="card">
                    <div class="cover  soft-sm--ends" style="background-image: url('icons/presentation/cloud.svg'); background-size: 100px; -webkit-transform: rotate(180deg); transform: rotate(180deg);">
                        <img src="icons/presentation/icons8-soccer_yellow_card.svg" width="48" style="-webkit-transform: rotate(180deg); transform: rotate(180deg);">
                    </div>
                    <div class="soft-lg  push-sm--top">
                        <h3 class="text--five  text--bold  text--uppercase  push-sm--bottom">Buy Credits</h3>
                        <div>Purchase ChargeNow credits via credit card or online transfer.</div>
                    </div>
                </div>
            </div>
            <div class="grid__item  one-quarter  palm-one-whole">
                <div class="card">
                    <div class="cover  soft-sm--ends" style="background-image: url('icons/presentation/cloud.svg'); background-size: 100px">
                        <img src="icons/presentation/icons8-near_me.svg" width="48">
                    </div>
                    <div class="soft-lg  push-sm--top">
                        <h3 class="text--five  text--bold  text--uppercase  push-sm--bottom">Navigate</h3>
                        <div>Locate nearby chargers and receive turn-by-turn directions.</div>
                    </div>
                </div>
            </div>
            <div class="grid__item  one-quarter  palm-one-whole">
                <div class="card">
                    <div class="cover  soft-sm--ends" style="background-image: url('icons/presentation/cloud.svg'); background-size: 100px; -webkit-transform: rotate(180deg); transform: rotate(180deg);">
                        <img src="icons/presentation/icons8-gas_station.svg" width="48" style="-webkit-transform: rotate(180deg); transform: rotate(180deg);">
                    </div>
                    <div class="soft-lg  push-sm--top">
                        <h3 class="text--five  text--bold  text--uppercase  push-sm--bottom">Start Charge</h3>
                        <div>Start charging by tapping the ChargeNow card on the charger.</div>
                    </div>
                </div>
            </div>
        </div>

        <div class="text--center  soft-md--top  push-xl--top">
            <p>You may register your new account using the web portal</p>

            <a href="register.php" class="btn  btn--secondary  text--uppercase" style="border-radius: 100px; padding: 12px 32px;">Register Now</a>
        </div>
    </div>
</section>
