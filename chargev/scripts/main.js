$(document).ready(function(){
    $(document).ready(function(){
        $(".main").onepage_scroll({
            sectionContainer: "section",
            animationTime: 1200,
            loop: false,

            beforeMove: function(index) {
                console.log('beforerMove ' + index);
                var prevIndex = index - 1;
                var nextIndex = index + 1;
                $('.js-slide-' + nextIndex).removeClass('loaded');
            },

            afterMove: function(index) {
                var loadedDelay = setTimeout(function() {
                    console.log('afterMove ' + index);
                    $('.js-slide').removeClass('loaded');
                    $('.js-slide-' + index).addClass('loaded');
                }, 100);
            }
        });
    });
});
