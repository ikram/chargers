<?php include '_template-header.php'; ?>

<section class="cover  relative" style="height: calc(100% - 160px); background-image: url('images/wallpaper-intro.jpg');">
    <?/*
    <div class="shade  absolute  pin"></div>

    <div class="container  relative" style="height: 100%;">
        <div class="one-whole  absolute  pin--bottom  pin--left  text--center" style="margin-bottom: 160px">
            <div class="milli  text--five  text--uppercase  push-md--bottom" style="color: rgba(255,255,255,.5)">EV Charging Made Easy</div>
            <h1 class="text--one  text--uppercases  weight--semibold" style="color: #FFF;">EV Charging Made Easy</h1>
            <div style="color: rgba(255,255,255,.75); font-size: 16px;">Access the largest network of public charging stations nationwide with a single card</div>
            <a href="register.php" class="btn  btn--secondary  text--uppercase" style="border-radius: 100px; padding: 12px 32px; margin-top: 32px">Register Now</a>
            <a href="#" class="btn  text--uppercase" style="border-radius: 100px; padding: 12px 32px; margin-top: 32px; color: rgba(255,255,255,.4); text-decoration: underline !important;">
                <span style="color: #FFF;">Stations Location</span>
            </a>
        </div>
    </div>
    */?>
</section>

<?php include '_slide-about.php'; ?>
<?php include '_slide-facts.php'; ?>
<?php include '_slide-news.php'; ?>
<?php include '_slide-steps.php'; ?>
<?php include '_slide-location.php'; ?>
<?php include '_slide-faq.php'; ?>
<?php include '_slide-contact.php'; ?>
<a href="#top" class="fixed  pin--bottom  pin--right  push-md  visuallyhidden--palm  js-back-top" style="display: none; z-index: 9999">Back to Top</a>
<?php include '_template-footer.php'; ?>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-animateNumber/0.0.14/jquery.animateNumber.min.js"></script>
<script>
// Select all links with hashes
$('a[href*="#"]')
// Remove links that don't actually link to anything
.not('[href="#"]')
.not('[href="#0"]')
.click(function(event) {
    // On-page links
    if (
        location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
        &&
        location.hostname == this.hostname
    ) {
        // Figure out element to scroll to
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        // Does a scroll target exist?
        if (target.length) {
            // Only prevent default if animation is actually gonna happen
            event.preventDefault();
            $('html, body').animate({
                scrollTop: target.offset().top
            }, 1000, function() {
                // Callback after animation
                // Must change focus!
                var $target = $(target);
                $target.focus();
                if ($target.is(":focus")) { // Checking if the target was focused
                    return false;
                } else {
                    $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
                    $target.focus(); // Set focus again
                };
            });
        }
    }


    // ===== Scroll to Top ====
    $(window).scroll(function() {
        if ($(this).scrollTop() >= 200) {        // If page is scrolled more than 50px
            $('.js-back-top').fadeIn(200);    // Fade in the arrow
        } else {
            $('.js-back-top').fadeOut(200);   // Else fade out the arrow
        }
    });
});



$(document).ready(function(){

    // car slides
    var carSlidesAnimation = function() {

        var $carSlides = $('.js-car-slides'),
        carSlidesOffset = $carSlides.offset().top,
        bindPosition = 450, //change this number to get the point where the car slide in effect should bind - 0 means straight away bind when element's top is visibile to screen
        viewPortHeight = $(window).height() - bindPosition, //get viewport height minus bindPosition
        numberIsAnimate = true;

        var windowScroll = $(window).scroll(function(){
            if(windowScroll.scrollTop() > (carSlidesOffset - viewPortHeight) && numberIsAnimate) {
                $carSlides.addClass('animate');

                $('.js-animate-number').each(function(){

                    var thisNumber = $(this).data('number');
                    var number_comma = $.animateNumber.numberStepFactories.separator(',');

                    $(this).animateNumber({
                        number : thisNumber,
                        numberStep : number_comma
                    }, 2000);
                });

                numberIsAnimate = false;
            }
        });
    }

    carSlidesAnimation();
});
</script>
