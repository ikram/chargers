<?php include '_template-header.php'; ?>

<section class="wrapper  push-xl--ends" style="padding: 40px 0; line-height: 20px;">
    <div class="container">
        <h1 class="text--three  text--bold">ChargEV Online Registration Form</h1>

        <div class="text--muted">Form for BMW PHEV or EV only. All fields are compulsory.</div>
    </div>

    <hr class="rule  rule--light" style="margin: 24px 0">

    <div class="container">
        <div class="grid">
            <div class="grid__item  one-third  palm-one-whole">
                <h3 class="text--five  text--light  text--uppercases">01. Personal Information</h3>
            </div>
            <div class="grid__item  two-thirds  palm-one-whole">
                <div class="form-group">
                    <label>Full Name</label>
                    <input type="text" class="input">
                    <div class="milli  text--muted  push-sm--top">
                        * as per MYKAD or Passport
                    </div>
                </div>

                <div class="form-group">
                    <label>Identification Number</label>
                    <input type="text" class="input">
                    <div class="milli  text--muted  push-sm--top">
                        * Use passport number for non-citizen
                        <br>
                        * Numbers only, no '-' or spaces.
                    </div>
                </div>
            </div>
        </div>
    </div>

    <hr class="rule  rule--light" style="margin: 24px 0">

    <div class="container">
        <div class="grid">
            <div class="grid__item  one-third  palm-one-whole">
                <h3 class="text--five  text--light  text--uppercases">02. Contact Information</h3>
            </div>
            <div class="grid__item  two-thirds  palm-one-whole">
                <div class="form-group">
                    <label>Mailing Address</label>
                    <textarea type="text" class="input" rows="3"></textarea>
                    <div class="milli  text--muted  push-sm--top">
                        * Please provide a valid mailing address for POSLaju card's delivery
                    </div>
                </div>

                <div class="grid">
                    <div class="grid__item  one-third  palm-one-whole">
                        <div class="form-group">
                            <label>Postcode</label>
                            <input type="text" class="input">
                        </div>
                    </div>

                    <div class="grid__item  one-third  palm-one-whole">
                        <div class="form-group">
                            <label>City</label>
                            <input type="text" class="input">
                        </div>
                    </div>

                    <div class="grid__item  one-third  palm-one-whole">
                        <div class="form-group">
                            <label>State</label>
                            <input type="text" class="input">
                        </div>
                    </div>
                </div>

                <hr class="rule  rule--light" style="margin: 16px 0">

                <div class="grid">
                    <div class="grid__item  one-third  palm-one-whole">
                        <div class="form-group">
                            <label>Phone Number</label>
                            <input type="text" class="input">
                            <div class="milli  text--muted  push-sm--top">
                                * Numbers only no '+', '-' or spaces.
                            </div>
                        </div>
                    </div>

                    <div class="grid__item  one-half  palm-one-whole">
                        <div class="form-group">
                            <label>Email Address</label>
                            <input type="text" class="input">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <hr class="rule  rule--light" style="margin: 24px 0">

    <div class="container">
        <div class="grid">
            <div class="grid__item  one-third  palm-one-whole">
                <h3 class="text--five  text--light  text--uppercases">03. Vehicle Information</h3>
            </div>
            <div class="grid__item  two-thirds  palm-one-whole">
                <div class="form-group">
                    <label>Vehicle Registration Number</label>
                    <input type="text" class="input">
                </div>

                <div class="form-group">
                    <label>Vehicle Model</label>
                    <input type="text" class="input">
                    <div class="milli  text--muted  push-sm--top">
                        * Model available for BMW only
                    </div>
                </div>

                <div class="form-group">
                    <label>Are you the owner of this vehicle?</label>
                    <label class="radio  radio--  palm-one-whole  push-xl--right">
                        <input type="radio" id="owner-yes" name="ownership">
                        <span class="icon"></span>
                        Yes, I owned this vehicle
                    </label>

                    <label class="radio  radio--  palm-one-whole  push-xl--right">
                        <input type="radio" id="owner-lease" name="ownership">
                        <span class="icon"></span>
                        No, I lease this vehicle
                    </label>

                    <label class="radio  radio--  palm-one-whole  push-xl--right">
                        <input type="radio" id="owner-no" name="ownership">
                        <span class="icon"></span>
                        No, this is a company vehicle
                    </label>
                </div>
            </div>
        </div>
    </div>

    <hr class="rule  rule--light" style="margin: 24px 0">

    <div class="container">
        <div class="grid">
            <div class="grid__item  one-third  palm-one-whole">
                <h3 class="text--five  text--light  text--uppercases">04. Business Arrangement</h3>
            </div>
            <div class="grid__item  two-thirds  palm-one-whole">
                <div class="form-group">
                    <label class="push-md--bottom">Please provide your preferred RFID card & car sticker collection arrangement.</label>

                    <label class="radio  radio--  palm-one-whole  push-xl--right">
                        <input type="radio" id="collect-self" name="collection">
                        <span class="icon"></span>
                        <div class="push-sm--bottom">Self-Collection</div>
                        The RFID card & the windscreen car sticker will be ready to collect within 10 working days. Visit GreenTech Malaysia office at No. 2 Jalan 9/10, Persiaran Usahawan, Seksyen 9, 43650 Bandar Baru Bangi Selangor from Monday to Friday, 9AM to 5PM (except lunch hour at 1-2PM) Please email or Whatsapp to 011-23373387 for collection your ChargeNOW card at our reception desk. Waze Malaysian Green Technology Corporation for navigation.
                    </label>

                    <hr class="rule" style="margin: 12px 0 12px 28px">

                    <label class="radio  radio--  palm-one-whole  push-xl--right">
                        <input type="radio" id="collect-post" name="collection">
                        <span class="icon"></span>
                        <div class="push-sm--bottom">Delivery via Post</div>
                        The RFID card & the windscreen car sticker will be delivered as per your registration address within 14 working days. Ensure your registered address is valid & there is someone to receivee the POSLaju delivery. Please note that the delivery chargers is RM15.00 and payment to be made to Malaysian Green Technology Corporation (account number 8600434621 CIMB Islamic Bank Berhad).
                    </label>
                </div>
            </div>
        </div>
    </div>

    <hr class="rule  rule--light" style="margin: 24px 0">

    <div class="container">
        <div class="grid">
            <div class="grid__item  one-third  palm-one-whole">
                <h3 class="text--five  text--light  text--uppercases">05. Terms and Condtions</h3>
            </div>
            <div class="grid__item  two-thirds  palm-one-whole">
                <div class="form-group">
                    <label class="push-md--bottom">
                        You must agree to the terms & conditions in order to become a ChargeNow member.
                        <br>
                        ChargeNow membership is FREE, NO annual fee & FREE to charge.
                    </label>

                    <label class="checkbox  palm-one-whole  push-xl--right">
                        <input type="checkbox" id="tnc-1">
                        <span class="icon"></span>
                        ChargeNow powered by ChargEV charging station does not come with Type 2 to Type 2 charging cable. Please use your personal Type 2 to Type 2 cable.
                    </label>

                    <hr class="rule" style="margin: 12px 0 12px 28px">

                    <label class="checkbox  palm-one-whole  push-xl--right">
                        <input type="checkbox" id="tnc-2">
                        <span class="icon"></span>
                        Email a scan copy (jpg or pdf format) of your valid driving license & MYKAD to chargEV@greentechmalaysia.my with "ChargeNow" and your MYKAD number & as the email subject. Example: ChargeNow 123456789012
                    </label>

                    <hr class="rule" style="margin: 12px 0 12px 28px">

                    <label class="checkbox  palm-one-whole  push-xl--right">
                        <input type="checkbox" id="tnc-3">
                        <span class="icon"></span>
                        Email your proof of payment for the delivery fee to chargEV@greentechmalaysia.my
                    </label>

                    <hr class="rule" style="margin: 12px 0 12px 28px">

                    <label class="checkbox  palm-one-whole  push-xl--right">
                        <input type="checkbox" id="tnc-4">
                        <span class="icon"></span>
                        I hereby certify that, to the best of my knowledge, all the information provided in this form is true and correct.
                    </label>

                    <hr class="rule" style="margin: 12px 0 12px 28px">

                    <label class="checkbox  palm-one-whole  push-xl--right">
                        <input type="checkbox" id="tnc-5">
                        <span class="icon"></span>
                        YES, I have read, understood and accept the terms and conditions.
                    </label>

                    <hr class="rule" style="margin: 12px 0 12px 28px">

                    <label class="checkbox  palm-one-whole  push-xl--right">
                        <input type="checkbox" id="tnc-6">
                        <span class="icon"></span>
                        YES, I have read and understood the GreenTech Malaysia Privacy Policy (Available at <a href="http://backup.greentechmalaysia.my/add/legal/privacy-policy" target="_blank">http://backup.greentechmalaysia.my/add/legal/privacy-policy</a> ), and I consent to my personal data, being processed and used by GreenTech Malaysia for the purpose indicated in the GreenTech Malaysia Privacy Policy.
                    </label>

                    <hr class="rule" style="margin: 12px 0 12px 28px">

                    <label class="checkbox  palm-one-whole  push-xl--right">
                        <input type="checkbox" id="tnc-7">
                        <span class="icon"></span>
                        YES, I would like to be kept informed about what ChargEV doing.
                    </label>

                    <hr class="rule" style="margin: 12px 0 12px 28px">

                    <label class="checkbox  palm-one-whole  push-xl--right">
                        <input type="checkbox" id="tnc-7">
                        <span class="icon"></span>
                        Email a scan copy of your car registration card to chargEV@greentechmalaysia.my
                    </label>
                </div>
            </div>
        </div>
    </div>

    <hr class="rule  rule--light" style="margin: 24px 0">

    <div class="container  text--center">
        <a href="#" class="btn  btn--secondary  text--uppercase" style="border-radius: 100px; padding: 12px 32px;">Submit My Application</a>
    </div>
</section>

<?php include '_template-footer.php'; ?>
