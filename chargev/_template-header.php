<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

    <!-- Page title for SEO -->
    <title>ChargEV Malaysia</title>
    <meta name="description" content="">

    <!-- Open Graph content -->
    <meta property="”og:title”" content="Chargenow">
    <meta property="og:description" content="Bleh bleh bleh.">
    <meta property="og:image" content="og-image.png">
    <link rel="stylesheet" href="styles/assets.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,500|Rubik:300" rel="stylesheet">
    <!-- <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,700" rel="stylesheet"> -->

</head>
<body class="text--light">
    <header class="header  text--center" id="top">
        <div class="container  text--left  soft-lg--ends">
            <div class="table  one-whole">
                <div class="table-cell  valign--middle  one-whole">
                    <a href="index.php">
                        <img src="images/logo-chargev.png" class="header__logo  header__logo--main  float--left  palm-two-fifhts" height="48" />
                    </a>
                </div>

                <div class="table-cell  valign--middle  one-whole  visuallyhidden--palm  space--nowrap">
                    <img src="images/logo-greentech.png" class="inline--block  valign--top  push-sm--top  palm-two-fifhts  visuallyhidden--palm" height="48" />
                </div>

                <div class="table-cell  valign--middle  visuallyhidden--lap-and-up">
                    <a href="javascript:void(0)" class="header__menu-mobile  push-xl--right  js-toggle-menu">
                        <span class="bars  relative  transition--default">
                            <span class="bar  bar--one  absolute  transition--default"></span>
                            <span class="bar  bar--two  absolute  transition--default"></span>
                            <span class="bar  bar--tri  absolute  transition--default"></span>
                        </span>
                    </a>
                </div>
            </div>
        </div>

        <div class="header__menu  transition--default">
            <div class="container">
                <ul class="menu  text--uppercase">
                    <li class="palm-one-whole"><a href="#about">About</a></li>
                    <li class="palm-one-whole"><a href="#news">News</a></li>
                    <li class="palm-one-whole"><a href="#getting-started">Getting Started</a></li>
                    <li class="palm-one-whole"><a href="#location">Locations</a></li>
                    <li class="palm-one-whole"><a href="#faq">FAQs</a></li>
                </ul>
            </div>
        </div>
    </header>
