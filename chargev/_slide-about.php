<section class="slide  slide-about" id="about" style="background: #FFF">
    <div class="container  text--center">
        <div class="push-xl--bottom  soft-sm--bottom">
            <h2 class="text--six  text--normal  text--muted  text--uppercase  push-xs--bottom" style="font-family: inherit; letter-spacing: 1px">About Us</h2>
            <h3 class="text--three  text--bold">
                An Electric Mobility Initiative by Greentech Malaysia
            </h3>
        </div>

        <p class="lead">With over 100 charging stations across Malaysia (as of October 2016), ChargEV provide plug-in cars with charging infrastructure and facilities across Malaysia.</p>

        <div class="block-inline  four-fifths">
            <img src="images/logo-box-greentech.png" height="120" class="push-xl--right">
            <img src="images/logo-box-chargev.png" height="120">
        </div>
    </div>
</section>
