<section class="slide  slide-contact" id="contact" style="background: #FFF">
    <div class="container  text--center">
        <div class="push-xl--bottom  soft-sm--bottom">
            <h2 class="text--six  text--normal  text--muted  text--uppercase  push-xs--bottom" style="font-family: inherit; letter-spacing: 1px">Contact Us</h2>
            <h3 class="text--three  text--bold">Lets get in touch!</h3>
        </div>

        <div class="block-inline  two-thirds">
            <div class="grid  grid--social  grid--steps  grid--lg">
                <div class="grid__item  grid--facebook  one-third  palm-one-whole">
                    <div class="card">
                        <div class="cover  soft-sm--ends" style="background-image: url('icons/presentation/cloud-2.svg'); background-size: 100px">
                            <img src="icons/presentation/icons8-facebook.svg" width="48">
                        </div>
                        <div class="text--muted  milli  push-sm--top">Facebook Page</div>

                        <a class="text--six" href="https://www.facebook.com/chargEV" target="_blank">
                            @chargEV
                        </a>
                    </div>
                </div>

                <div class="grid__item  grid--twitter  one-third  palm-one-whole">
                    <div class="card">
                        <div class="cover  soft-sm--ends" style="background-image: url('icons/presentation/cloud-2.svg'); background-size: 100px; -webkit-transform: rotate(180deg); transform: rotate(180deg);">
                            <img src="icons/presentation/icons8-twitter.svg" width="48" style="-webkit-transform: rotate(180deg); transform: rotate(180deg);">
                        </div>
                        <div class="text--muted  milli  push-sm--top">Twitter Page</div>
                        <a class="text--six" href="https://twitter.com/charg_ev" target="_blank">
                            @charg_EV
                        </a>
                    </div>
                </div>

                <div class="grid__item  grid--phone  one-third  palm-one-whole">
                    <div class="card">
                        <div class="cover  soft-sm--ends" style="background-image: url('icons/presentation/cloud-2.svg'); background-size: 100px">
                            <img src="icons/presentation/icons8-phone.svg" width="48">
                        </div>
                        <div class="text--muted  milli  push-sm--top">Call Us</div>
                        <a class="text--six" href="tel:+60389210800" target="_blank">
                            +603-8921 0800
                        </a>
                    </div>
                </div>

                <div class="grid__item  grid--website  one-half  palm-one-whole">
                    <div class="card">
                        <div class="cover  soft-sm--ends" style="background-image: url('icons/presentation/cloud-2.svg'); background-size: 100px; -webkit-transform: rotate(180deg); transform: rotate(180deg);">
                            <img src="icons/presentation/icons8-domain.svg" width="48" style="-webkit-transform: rotate(180deg); transform: rotate(180deg);">
                        </div>
                        <div class="text--muted  milli  push-sm--top">Our Website</div>
                        <a class="text--six" href="http://www.greentechmalaysia.my" target="_blank">
                            greentechmalaysia.my
                        </a>
                    </div>
                </div>

                <div class="grid__item  grid--email  one-half  palm-one-whole">
                    <div class="card">
                        <div class="cover  soft-sm--ends" style="background-image: url('icons/presentation/cloud-2.svg'); background-size: 100px">
                            <img src="icons/presentation/icons8-mailbox_closed_flag_up.svg" width="48">
                        </div>
                        <div class="text--muted  milli  push-sm--top">Email Us</div>
                        <a class="text--six" href="mailto:chargev@greentechmalaysia.my" target="_blank">
                            chargev@greentechmalaysia.my
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <div class="push-xl--top  soft-xl--top  text--muted  text--uppercase  micro">
            &copy; 2018 All rights reserved
        </div>
    </div>
</section>
