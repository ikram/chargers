<section class="slide  slide-facts">
    <div class="container  text--center">
        <div class="push-xl--bottom  soft-sm--bottom">
            <h2 class="text--six  text--normal  text--muted  text--uppercase  push-xs--bottom" style="font-family: inherit; letter-spacing: 1px">A strong partnership</h2>
            <h3 class="text--three  text--bold">Empowering Electric Mobility</h3>
        </div>


        <div class="cars--charging  relative  js-car-slides">
            <img class="cars--mercedes  absolute" src="images/merc-c350.png" alt="mercedes-350">
            <img class="cars--volvo  absolute" src="images/volv-xc90.png" alt="volvo-xc90">
            <img class="cars--hyundai  absolute" src="images/hyundai-ioniq.png" alt="hyundai-ioniq">
            <img class="cars--nissan  absolute" src="images/niss-leaf.png" alt="nissan-leaf">
            <img class="charging--station  relative  z-index  tier--one" src="images/charging-station.png" alt="charging-station">
            <img class="cars--tesla  absolute  tier--two" src="images/tes-models.png" alt="tesla">
        </div>


        <div class="grid  grid--facts  grid--full">
            <div class="grid__item  one-quarter  soft-xl  palm-one-whole">
                <h3 class="text--six  text--muted  text--uppercase  push-sm--bottom">Public EV Chargers</h3>
                <div class="text--one  palm-text--three  text--light js-animate-number" data-number="50">0</div>
                <div class="milli  text--muted">Stations</div>
            </div>

            <div class="grid__item  one-quarter  soft-xl  palm-one-whole  grid--facts__border-left">
                <h3 class="text--six  text--muted  text--uppercase  push-sm--bottom">Public EV Charging</h3>
                <div class="text--one  palm-text--three  text--light  js-animate-number" data-number="31">0</div>
                <div class="milli  text--muted">Location available in Malaysia</div>
            </div>

            <div class="grid__item  one-quarter  soft-xl  palm-one-whole  grid--facts__border-left">
                <h3 class="text--six  text--muted  text--uppercase  push-sm--bottom">EV Charging Sessions</h3>
                <div class="text--one  palm-text--three  text--light js-animate-number" data-number="3200">0</div>
                <div class="milli  text--muted">Sessions</div>
            </div>

            <div class="grid__item  one-quarter  soft-xl  palm-one-whole  grid--facts__border-left">
                <h3 class="text--six  text--muted  text--uppercase  push-sm--bottom">Electricity Charged</h3>
                <div class="text--one  text--light js-animate-number" data-number="13250">0</div>
                <div class="milli  text--muted">Kilowatt hour</div>
            </div>
        </div>
    </div>

    <img src="images/wave.svg" style="width: 100%; max-width: 200%; display: none">
</section>
