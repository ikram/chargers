<section class="wrapper  wrapper--dark  ----------  visuallyhidden">
    <div class="container  container--large  text--center">
        <h3 class="text--five  text--muted  text--light  text--uppercases  flush">Hit the road, we are nearby</h3>
        <h2 class="text--three  text--white  text--normal  flush">It’s easy to get started, register today</h2>

        <a href="register.php" class="btn  btn--secondary  text--uppercase" style="border-radius: 100px; padding: 12px 32px; margin: 24px 0 0">Register Now</a>

        <hr class="rule" style="margin: 40px 0">

        <div class="grid  grid--full  soft-xl--ends">
            <div class="grid__item  one-third  text--left  palm-one-whole  palm-text--center">
                <a href="#" class="push-md--right"><img src="images/logo-facebook.svg" height="16px"></a>
                <a href="#" class="push-md--right"><img src="images/logo-twitter.svg" height="16px"></a>
                <br class="visuallyhidden--lap-and-up">
                <br class="visuallyhidden--lap-and-up">
            </div>
            <div class="grid__item  one-third  text--center  palm-one-whole  palm-text--center">
                <img src="images/logo-chargenow-white.svg" class="inline--block  valign--top" height="24" />
                <br class="visuallyhidden--lap-and-up">
                <br class="visuallyhidden--lap-and-up">
            </div>
            <div class="grid__item  one-third  text--right  palm-one-whole  palm-text--center">&copy; 2017 All rights reserved</div>
        </div>
    </div>
</section>

<script src="scripts/framework/jquery-1.9.1.min.js"></script>

<script>
    $('.js-toggle-menu').click(function() {
        $('body').toggleClass('show--menu');
    });
</script>
</body>
