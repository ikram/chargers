<section class="slide  slide-steps" id="faq">
    <div class="container  text--center">
        <div class="push-xl--bottom  soft-sm--bottom">
            <h2 class="text--six  text--normal  text--muted  text--uppercase  push-xs--bottom" style="font-family: inherit; letter-spacing: 1px">Frequently asked questions</h2>
            <h3 class="text--three  text--bold">What to expect along the way</h3>
        </div>

        <div class="accordian">
            <input type="checkbox" id="checkbox-faq-01" class="accordian__toggle  visuallyhidden">
            <label class="accordian__handler" for="checkbox-faq-01">
                Which charging stations are part of the ChargeNow network?
                <span></span>
            </label>
            <div class="accordian__content  soft-md--top">As a ChargeNow customer, you use all charging stations in the ChargeNow network. By continuously expanding the partnerships and increasing the number of charging stations in the existing ChargeNow, the ChargeNow network and therefore the charging station coverage will be continuously expanded. With the charging station finder you can see all ChargeNow charging stations at chargenow.com.</div>
        </div>

        <div class="accordian">
            <input type="checkbox" id="checkbox-faq-02" class="accordian__toggle  visuallyhidden">
            <label class="accordian__handler" for="checkbox-faq-02">
                How do I find a suitable charging station in the network of ChargeNow charging stations?
                <span></span>
            </label>
            <div class="accordian__content  soft-md--top">As a ChargeNow customer, you use all charging stations in the ChargeNow network. By continuously expanding the partnerships and increasing the number of charging stations in the existing ChargeNow, the ChargeNow network and therefore the charging station coverage will be continuously expanded. With the charging station finder you can see all ChargeNow charging stations at chargenow.com.</div>
        </div>

        <div class="accordian">
            <input type="checkbox" id="checkbox-faq-03" class="accordian__toggle  visuallyhidden">
            <label class="accordian__handler" for="checkbox-faq-03">
                How do I become a ChargeNow customer?
                <span></span>
            </label>
            <div class="accordian__content  soft-md--top">As a ChargeNow customer, you use all charging stations in the ChargeNow network. By continuously expanding the partnerships and increasing the number of charging stations in the existing ChargeNow, the ChargeNow network and therefore the charging station coverage will be continuously expanded. With the charging station finder you can see all ChargeNow charging stations at chargenow.com.</div>
        </div>

        <div class="accordian">
            <input type="checkbox" id="checkbox-faq-04" class="accordian__toggle  visuallyhidden">
            <label class="accordian__handler" for="checkbox-faq-04">
                I do not have a driving license. Can I still become a customer of ChargeNow?
                <span></span>
            </label>
            <div class="accordian__content  soft-md--top">As a ChargeNow customer, you use all charging stations in the ChargeNow network. By continuously expanding the partnerships and increasing the number of charging stations in the existing ChargeNow, the ChargeNow network and therefore the charging station coverage will be continuously expanded. With the charging station finder you can see all ChargeNow charging stations at chargenow.com.</div>
        </div>

        <div class="accordian">
            <input type="checkbox" id="checkbox-faq-05" class="accordian__toggle  visuallyhidden">
            <label class="accordian__handler" for="checkbox-faq-05">
                Which charging stations are part of the ChargeNow network?
                <span></span>
            </label>
            <div class="accordian__content  soft-md--top">As a ChargeNow customer, you use all charging stations in the ChargeNow network. By continuously expanding the partnerships and increasing the number of charging stations in the existing ChargeNow, the ChargeNow network and therefore the charging station coverage will be continuously expanded. With the charging station finder you can see all ChargeNow charging stations at chargenow.com.</div>
        </div>

        <div class="accordian">
            <input type="checkbox" id="checkbox-faq-06" class="accordian__toggle  visuallyhidden">
            <label class="accordian__handler" for="checkbox-faq-06">
                How do I find a suitable charging station in the network of ChargeNow charging stations?
                <span></span>
            </label>
            <div class="accordian__content  soft-md--top">As a ChargeNow customer, you use all charging stations in the ChargeNow network. By continuously expanding the partnerships and increasing the number of charging stations in the existing ChargeNow, the ChargeNow network and therefore the charging station coverage will be continuously expanded. With the charging station finder you can see all ChargeNow charging stations at chargenow.com.</div>
        </div>

        <div class="accordian">
            <input type="checkbox" id="checkbox-faq-07" class="accordian__toggle  visuallyhidden">
            <label class="accordian__handler" for="checkbox-faq-07">
                How do I become a ChargeNow customer?
                <span></span>
            </label>
            <div class="accordian__content  soft-md--top">As a ChargeNow customer, you use all charging stations in the ChargeNow network. By continuously expanding the partnerships and increasing the number of charging stations in the existing ChargeNow, the ChargeNow network and therefore the charging station coverage will be continuously expanded. With the charging station finder you can see all ChargeNow charging stations at chargenow.com.</div>
        </div>

        <div class="accordian">
            <input type="checkbox" id="checkbox-faq-08" class="accordian__toggle  visuallyhidden">
            <label class="accordian__handler" for="checkbox-faq-08">
                I do not have a driving license. Can I still become a customer of ChargeNow?
                <span></span>
            </label>
            <div class="accordian__content  soft-md--top">As a ChargeNow customer, you use all charging stations in the ChargeNow network. By continuously expanding the partnerships and increasing the number of charging stations in the existing ChargeNow, the ChargeNow network and therefore the charging station coverage will be continuously expanded. With the charging station finder you can see all ChargeNow charging stations at chargenow.com.</div>
        </div>
    </div>
</section>
